# [manifest-app](https://bitbucket.org/robertograham/manifest-app/)

## Usage

### Run with [docker-app](https://github.com/docker/app)

Overriding of app published port supported with `client-published-port`

Overriding of server's container name supported with `server-container-name`

```bash
docker-app render robertograham/manifest | docker-compose -f - up
```